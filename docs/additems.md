# Add items to your storage

We assume that you have already created one or more storage facilities as described in [Getting Started](index.md#getting-started).

The basic idea of dingsda is: Everything is a thing. So there is no big difference between containers and other items. Storage Facilities are also just items with one special feature: They have a [geolocation](location.md#geolocation). That and only that makes them a Storage Facility.

Items, as we will add in the next steps, differ in so far that they do not have a [geolocation](location.md#geolocation). This is because they move around all the time. It's a thing that things do. Really!
But we still need to find items, right? This is why items always have a [location](location.md#inside-of--container-location) as well! Usually you find items in other items. That can be: Inside of a Storage Facility (you probably have some shelves there for example). But an item can also be inside of another item. This is why the location of an item is sometimes also referred to as the [inside-of location](location.md#inside-of--container-location) For example: You might have a box full of cables on a shelf inside your Storage Facility.

Let's do a practical example:

1. Grace has their storage facility (**Grace's Main Storage**) already set up: It can be found on the Startscreen as well as via the search function. But nothing is inside yet:

    <img src="img/image-5.png" style="height:400px" /> <img src="img/image-6.png" style="height:400px" />

2. Now Grace wants to add a shelf to their Main Storage: They click on the little camera icon in the menu bar and the Add Item Form opens:

    <img src="img/image-7.png" style="height:400px" />

3. In the search field **Storage** they look for their **Main Storage** by name and pick it from the Autocomplete's Dropdown:

    <img src="img/image-8.png" style="height:400px" /><img src="img/image-9.png" style="height:400px" />

    !!! success "PRO-TIP"
        Alternatively they could also scan a QR Code identifying their Main Storage by clicking on the QR code icon next to the Storage search field.
        
        **OR** they could use a connected USB-QR Code or Barcode Reader to fill in the UID of the Main Storage. 
        
        **OR** they could pick their Main Storage from the Search View or the Startscreen and then pick `... > Add items inside`.
        
        dingsda 2mex adapts well to different workflows!

4. Next, they fill out the other fields in the form.

    [**Name**](itemfields.md#name), [**location**](location.md) and [**owners**](itemfields.md#owners--owned-by) are mandatory for every item. 
    
    The rest is optional. And [**owners**](itemfields.md#owners--owned-by) is **automatically prefilled** with your account id but can be edited if you for example want to add items owned by a group you are a member of or if you share ownership with other users. 

    <img src="img/image-10.png" style="height:400px"  />

5. (optional) Grace can also **take** or **upload** one or more **pictures** to make it easier to identify the shelf, when looking around in the Storage Facility

    <img src="img/image-11.png" style="height:400px"  /> <img src="img/image-13.png" style="height:400px" />

6. (optional) Grace can also add [**tags**](itemfields.md#tags), labels that will help later find and sort items in their dingsda database. Tags work via an autocomplete dropdown menu that offers all existing tags matching the pattern typed into the input field. How you add tags to dingsda is explained in the [tags section](tags.md).

    <img src="img/image-16.png" style="height:100px"  />

7. Excellent! Now Grace only has to scroll down and hit **ADD ITEM**. And... drumroll..... They see the Success Screen now, which means the item has been successfully added to the Storage Facility! 

    !!! success "Different ways to find your item"
        Want proof? 
        
        Open the **Search View** and search for it! 
        
        <img src="img/image-12.png" style="height:250px"  />

        OR: open **Grace's Main Storage** in the **Item Detail View** and find it under `contains` which lists all items inside the item.

        <img src="img/image-14.png" style="height:250px"  /> <img src="img/image-15.png" style="height:250px"  />
    
