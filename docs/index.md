---
hide:
  - navigation
---

# dingsda2mex tutorials and user documentation

<img src="https://commons.machinaex.org/img/dingsda-logo-detail-bunt_web_klein.png" width=50%>

Welcome to the dingsda 2mex tutorials.

Here we hope to give you a simple to follow introduction into the functionalities of dingsda2mex.

If you have further questions, don't hesitate to ask us on our [discord server](https://discord.gg/quHbQAMvF6)


## Installation

dingsda 2mex is Open Source. You can install it yourself on your own server. Please follow the installation instructions in the gitlab repository for that. You will need quite a bit of IT knowledge and the instructions might not always be up to date. Feel free to ask for help on the [discord server](https://discord.gg/quHbQAMvF6). 

You can of course also ask someone there to install it for you.

## Getting started

You have a dingsda 2mex server instance running or get access to one and want to get started? Great!

First step after getting your own account will be to create your first Storage-Location:

- 1) Click on ADD Storage Location. The Add Item View will open:

- 2) Your Storage Facility needs at least a name and a Location. For the location you can either click on the little location button or type in an address and let the autocomplete take care of you. If you don't want to do either of those, you can also just type in the latitude and longitude directly.

    <img src="img/image.png" style="height:400px" />
    <img src="img/image-1.png" style="height:400px" />

- 3) Scroll down and click "ADD"

    <img src="img/image-2.png" style="height:400px" />
    <img src="img/image-3.png" style="height:400px" />

- 4) Now you see a success message and the Success Screen. Congratulations! You created your first dingsda 2mex storage facility! You can find it now listed on your Profile and Startscreen:

    <img src="img/image-4.png" style="height:400px" />
    <img src="img/image-5.png" style="height:400px" />

    Now it is time to fill it!

5) Go to [Add Items](additems.md)



## Authors and acknowledgment


This is a machina commons project by [machina eX](https://machinaex.com).

Maintained by machina eX and Philip Steimel

<br/>

gefördert durch

<img src="https://commons.machinaex.org/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="50%"/>

Senatsverwaltung für Kultur und Europa Berlin


