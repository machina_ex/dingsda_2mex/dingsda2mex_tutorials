# Item Operations

Items can be changed and edited in different ways. 
You can access the item operations in the [Item Detail Screen](itemDetail.md) or in the [Basket](basket.md) by clicking on the context menu (⋮) button.

dingsda 2mex knows the following operations:

- **Add to Basket** (*ItemDetail only*): will add the item to your basket.
- **Search Inside**  (*ItemDetail only*): will open the search screen with the item set as the location of the search (this is assuming you are using the item as a storage container).
- **Add Items**  (*ItemDetail only*): Opens the [Add Items Screen](additems.md) but fills out the location of the new item with this item as its container.
- **Edit**  (*ItemDetail only*): Opens the [Edit Screen](edit.md) for this item.
- **Delete (ALL) FROM DB**: ATTENTION: this will delete all items in your basket or the opened item in the item Detail Screen from your database. They will be lost forever!
- **Reserve**: this will open the reservation screen and let you reserve items (as long as you own them) for a given timeframe.
- **Handover**/**Take back**: this will handover all items (as long as you own them) to another user or user group. Type their user id into the text field and click handover. Take back will take back all items in the list that are owned by you but in the possession of other users or groups.
- **Move**: this will move all items in your basket to a different location. The location can be either: another item (use it's Item ID or scan its barcode) or a geolocation (choose it on the map or via address)
- **QR Code(s)**: This lets you create QR codes for all items in your basket. QR codes can identify items with the QR Code Scanner.
- **Create List** (Export as CSV, *BASKET ONLY*): This lets you export the basket as a CSV file (a simple spreadsheet) so you can use it in office programs or for transport lists etc. (this option can also be directly accessed via the document icon next to the context menu)
- **Empty Basket** (*BASKET ONLY*): will empty your basket
- **Create Copy**  (*ItemDetail only*): will open the [Add Item Screen](additems.md) but will fill it out with an exact copy of this item. Really great for quick data entry with similar items.

