# Tags 

**tags** are labels that help to organize, find and sort items in dingsda.

## Add tags to items
You can add tags in the **Add Item View** and in the **Edit Item View**
Tags work via an autocomplete dropdown menu that offers all existing tags matching the pattern typed into the input field.

<img src="img/image-16.png" style="height:100px"  />


## Create tags
Tags are unified per dingsda server instance. That means: Every user can use the same tags on the whole dingsda 2mex server.

But to see the tags as option in the [tags field's autocomplete](#add-tags-to-items), they have to be added to the database.

Depending on the configuration of your dingsda 2mex server instance, users can either 

- [**DIRECT TAG CREATION**](#direct-tag-creation): directly add their own tags by typing them into any [tags field](#add-tags-to-items) and hit Enter/Return to create them **or**
- [**ADMIN TAG CREATION**](#admin-tag-creation) *(recommended)* this feature is blocked and the server admin has to add them directly to the SQL database into the tags table.

After that, a new tag will be visible to all users using the [tags field's autocomplete](#add-tags-to-items)

### Direct tag creation

If your server and frontend configuration allows it (see below), you can just type a new tag into the `tags / specifics` field and hit enter. Your tag will be created (if it does not exist already) and will be offered to all users on your server in the autocompletes and search screens.

#### Enable/Disable Direct tag creation

!!! warning "For Admins"


to create tags directly, the dingsda server and the dingsda frontend have to allow this by setting the **environment variable** `DIRECT_TAG_ADDING_ALLOWED=true` (on the server) and `VUE_APP_DIRECT_TAG_ADDING_ALLOWED=true` (before building or serving the ). The server admin can do this as explained in the Install and Configuration Guide in the [Server Repository README](https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_backend#create-env-file) and the [Frontend Repository README](https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend#project-setup)

### Admin tag creation

!!! warning "For Admins"

If Direct tag creation is not allowed or you are working directly in the database anyhow, you can add new tags via SQL like this to the PostgreSQL database:

```sql
INSERT INTO tags (id,tag)
VALUES(uuid_generate_v4(),'myNewTag');
```

This method is recommended to avoid too many tags as well as typos etc.
A common method on small servers with a small group of users, is to keep it open on the first days of data entry and then correct and summarize tags and disable the direct tag creation afterwards to enforce the use of the existing tags
