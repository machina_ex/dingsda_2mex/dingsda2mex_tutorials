# Search for items

You can search for items inside your dingsda 2mex server.
You find items that you or a group you are a member of own or items that are set as visible for you or your groups.

## How to search

1. Open the Search View by clicking on the looking glass icon.
    
    dingsda 2mex first shows you all items owned by you or groups you are part of.

2. You can now either [filter the displayed items](#filter-search-results) and/or [search items by text](#search-by-text)

!!! success "PRO-TIP:"
    If you search by a Geolocation, you will also get items that are not owned by you, but made visible to you or the public by users on the same server. If you work directly with the items shown to you with a fresh search without a search query in the input field, you will only search your and your group's items.

### Search by text

1. type the name of an item you are looking for into the search field (4 letters minimum) and hit the **Search** button or **Enter/Return**

    dingsda 2mex will now show you only items that match your search query in their name.

    You can now scroll along to find the items or narrow down the search by [filtering the displayed items](#filter-search-results)


### Filter search results

You can filter the displayed search results further down, by different methods:

- [Filter by Tags and values](#filter-by-tags-and-values) (click on **Filters**)
- [Filter by Storage Location](#filter-by-storage-location) (click on **Location** > click on all locations you want to include in your search)
- [Filter by Geographic Location](#filter-by-geolocation) (click on **Location** > pick a place on the map and a radius)

#### Filter by tags and values
Let's for example look at the search results of a text search for `Audio`:

<img src="img/image-17.png" style="height:400px" />

We got far too many items that have `Audio` somewhere in their name. So we need to filter them down because we don't want to scroll through a long list of items.

We can 

1. click now on **Filters** in the upper left

    <img src="img/image-18.png" style="height:100px"/>

    Now we see all [tags](tags.md) that are used by any of the items in our current search:
    
    <img src="img/image-20.png" style="height:100px" />  <img src="img/image-19.png" style="height:400px"  />

2. click on all [tags](tags.md) that you want to include in the search results. Additionally you can also include value based filters for length, weight, etc, by using the sliders.

    <img src="img/image-22.png" style="height:100px" />
    
    here we want to only include items that have an Audio tag AND are a box

3. Go back to the Search View by clicking on the **X** in the upper right corner. Dingsda will do a new search including your filters and present you the findings:

    <img src="img/image-23.png" style="height:400px" />

### Filter by storage location

If you want to filter for items inside of one or serveral storage locations, you can

1. click on **Location** and choose from all locations/storages listed inside.

    <img src="img/image-24.png" style="height:200px" />

2. Your search will now combine filters and look only in marked storage locations:

    <img src="img/image-25.png" style="height:400px" />

### Filter by geolocation

If you want to look for items in a specific geographic area, the search will include items that are visible to you and your groups (including public items).

1. click on **Location** and choose a point on the map or search for an address in the input field.

2. define the radius around the point on the map to search in

    <img src="img/image-26.png" style="height:400px" />

3. Your search will now include not just items owned by you but all items visible to you and your groups in the given area (as long as the items exist on the same server and are set to be visible to you and/or public)


## Select items from search results.

To put the found items into your [Item Basket](basket.md), hit the **SELECT** button and choose the items in the Select Mode.

<img src="img/image-28.png" style="height:400px" />
<img src="img/image-27.png" style="height:400px" />

## Public items

public items, are items that have been marked as visible for the public (via item creation or item edit) by adding the public user `*` (asterisk) the items **visible to** field. 

<img src="img/image-29.png" style="height:200px" /> 

They will be displayed in the item details with a `visible:public` pill: 

<img src="img/image-30.png" style="height:200px" />

All public items will appear in every users searches that are done with a [geolocation filter](#search-by-geolocation) and therefore be publicly visible.

If your server is also configured to exchange data with other services, those items might also be visible to non users via other platforms or direct link.



