# Edit Items

You can edit items in the edit screen. It looks very much like the [add items screen](additems.md), but will of course edit an already existing item instead of creating a new one.

## Owners, Visible, Possession 
To not accidentally make edits that we did not want to do, the three permission fields are protected with an extra click.

If you want to edit the owners of an item, the visibility or the possessors of an item, click first on the pills listing those and a form will appear.