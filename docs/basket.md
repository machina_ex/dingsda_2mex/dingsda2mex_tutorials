# (Item) Basket

The **basket** view gives you the possibilty to save more than one items into a list in order to either 

- do [operations on all items in the basket](#bulk-item-operations) or
- export the list as a CSV (#export-csv) (a simple spreadsheet)

## Bulk Item Operations
Some item operations can be done in bulk. You can access them, by clicking on the context menu (⋮) and then choosing one of the following options:

- **Delete ALL FROM DB**: ATTENTION: this will delete all items in your basket from your database. They will be lost forever!
- **Reserve**: this will open the reservation screen and let you reserve items (as long as you own them) for a given timeframe.
- **Handover**/**Take back**: this will handover all items (as long as you own them) to another user or user group. Type their user id into the text field and click handover. Take back will take back all items in the list that are owned by you but in the possession of other users or groups.
- **Move**: this will move all items in your basket to a different location. The location can be either: another item (use it's [Item ID](itemfields.md#id) or scan its barcode) or a [geolocation](location.md#geolocation) (choose it on the map or via address)
- **Create List** (Export as CSV): This lets you export the basket as a CSV file (a simple spreadsheet) so you can use it in office programs or for transport lists etc. (this option can also be directly accessed via the document icon next to the context menu)
- **QR Codes**: This lets you create QR codes for all items in your basket. QR codes can identify items with the QR Code Scanner.
- **Empty Basket**: will empty your basket

Edit operations have to still be done on a per item basis. We hope to provide bulk item edits sometime in the future.

