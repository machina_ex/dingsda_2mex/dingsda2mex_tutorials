# Item fields

every item has several fields that can be either assigned in the [Add Item Screen](additems.md) or directly in the database.

Some fields are automatically generated by dingsda and can not be edited or set by a user. To distinguish them, the following list marks user facing fields with `found in Editor` 

## ID

the ID of an item is automatically assigned.

In the editor, users can find it in the URL of the [Item Detail Screen](itemDetail.md) of an item.
![alt text](img/image-33.png)

## Name

The name of an item. Usually we recommend using short names that you would search for. We often also include Model names of store bought items.

## insideOf
`found in Editor`
The [Storage Location](location.md#inside-of--container-location) of an item. Must be the ID of its' container.

## location
`found in Editor`

The [Geolocation of an item](location.md#geolocation). In the database more complex, but user facing is only the geograpy and radius.

## owners / owned by
`found in Editor`

the owners of an item. A list of user ids. 
Whoever is on this list can edit, move and delete the item.

## in possession of / lent to
`found in Editor`

the current possessor(s) of an item if not with the owners. This field is automatically filled out when an item is handed over or taken back to/from another user borrowing it.

## visible to / visible2
`found in Editor`

the users that can see this item and find it in searches even though they are not its owners or possessors.

if the special group `*` is included, the item is publicly visible and can be found by everybody, possibly even outsiders, if your dingsda server shares data with other sharing services.

## weight, length (depth), width, height, count
`found in Editor`

basic item properties.

## tags
`found in Editor`
search labels, search tags.

more at [tags](tags.md)