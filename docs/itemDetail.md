# Item Detail Screen

The item Detail Screen shows all the data of your item in one screen.

You can also click the context menu (⋮) to [edit the item](edit.md) or do other [item operations](itemOperations.md) with it.

![alt text](img/image-31.png)

## clickable areas
You can navigate through your items from the Item Detail Screen:

- a click on a location pill will bring you directly to that item.
- a click on a pill in the contains list (only displayed when this item is a container), will bring you to the item inside this item.
- a click on a tag under Specifics/Tags (only displayed if the item has tags associated) will open the [search screen] with the tag preselected