# Location 

The main idea of dingsda is: Things are inside of other things. So the location of most items inside of dingsda 2mex is usually: another item. But there are some exceptions as well as different ways to talk about where an item is located:

The **location** of an item can be/mean one of two things:

- if the item is inside of another item, it has an [inside of or container location](#inside-of--container-location).
- if the item has a [Geolocation](#geolocation), it cannot be inside another item and is a [Storage Location](index.md#getting-started)

## Inside of / Container location
Items are often found inside of other items: a theater light is stored in a box which itself is stored in a shelf which is inside of the theater storage space.

Nearly every item in dingsda is inside of another item. 

![](img/image-32.png)

By Setting the item Storage location to the ID of another item, the other item becomes its' container.
You can do that by either typing the name of the item into the Storage Location Box and choosing from the offered autocompletes **or** by typing or copying the [ID of an item](itemfields.md#id)

!!! note "Admin Info"
    In the database they are linked by the contained item pointing to its' container with the field `insideOf` being set to the ID string of the container.

Every item that does not have a inside of location, must have a [geolocation](#geolocation) instead and automatically becomes a [Storage Facility](index.md)


## Geolocation

The geographic location of an item. Most items take over the geolocation from the Storage Facility they, or their containers are located in. 

[Storage Facilities](index.md#getting-started) are the only items that have an explicitly set geolocation. You can find out in the [Getting Started Tutorial](index.md#getting-started) how to set it. Or you can edit an item and click on the little house icon next to `Storage` in the [Add item Screen](additems.md) to get to the geolocation fields of an item.